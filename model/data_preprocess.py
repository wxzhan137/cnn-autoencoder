import pandas as pd
from pandas import DataFrame
import numpy as np

# read txt
def read3Dtxt(file_name, column_name):
    print("Reading ", file_name)
    file = open(file_name, 'r')
    dim_line = -1
    data = []
    for line in file:
        if line.strip() == "6291456":
            print("Dimmension should be (6291456, 3)")
            dim_line = 1
        else:
            if dim_line>=2:
                line = line.strip()
                line = line.replace("(", "")
                line = line.replace(")", "")
                val_str = line.split()
                val = []
                for v in val_str:
                    val.append(float(v))
                data.append(val)
                if len(data)>=6291456:
                    break
            if dim_line>0:
                dim_line+=1
    file.close()
    df = DataFrame(data, columns=column_name)
    print("data shape is ", df.shape)
    if len(df)==6291456:
        print("Data dimension matches")
    else:
        print("Data dimension doesnt match")
        sys.exit(1)
    return df


def read1Dtxt(file_name, column_name):
    print("Reading ", file_name)
    file = open(file_name, 'r')
    dim_line = -1
    data = []
    for line in file:
        if line.strip() == "6291456":
            print("Dimmension should be (6291456, 1)")
            dim_line = 1
        else:
            if dim_line>=2:
                line = line.strip()
                val = float(line)
                data.append(val)
                if len(data)>=6291456:
                    break
            if dim_line>0:
                dim_line+=1
    file.close()
    df = DataFrame(data, columns=column_name)
    print("data shape is ", df.shape)
    if len(df)==6291456:
        print("Data dimension match")
    else:
        print("Data dimension doesnt match")
        sys.exit(1)
    return df
## read the C file
c_file = '/Users/wzhan/PycharmProjects/Tubulent_inflow/2000/C'
c_df = read3Dtxt(c_file, ["x", "y", "z"])
## read the v file
v_file = '/Users/wzhan/PycharmProjects/Tubulent_inflow/2000/U'
v_df = read3Dtxt(v_file, ["Vx", "Vy", "Vz"])
## read the p file
p_file = '/Users/wzhan/PycharmProjects/Tubulent_inflow/2000/p'
p_df = read1Dtxt(p_file, ["p"])

# combine it to a panda data frame
raw_data = pd.concat([c_df, v_df, p_df], axis=1)
print(raw_data.shape)

# sort base on the first column,x
raw_data.sort_values(["x",'y','z'], axis=0, ascending=[True,True,True], inplace=True)
x = raw_data[["x"]]


# get the last 4 column of data which is v and p
data = raw_data[["Vx", "Vy", "Vz", "p"]]
data_np = data.to_numpy()
# Data reshape, the idea shape is [256(x), 96(y), 256(z), 4], current data shape is (6291456, 4) and sorted based on x, y, z
data_reshape1 = []
for i in range(256):
    one_section_size = 96*256
    one_section = data_np[i*one_section_size:(i+1)*one_section_size, :]
    data_reshape1.append(one_section)
data_reshape1 = np.array(data_reshape1)
print("step 1 reshaped size", data_reshape1.shape)
data_resize = np.reshape(data_reshape1, [256, 96, 256, 4])
print("step 2 reshaped size", data_resize.shape)
# print(data_resize[])

np.save("/Users/wzhan/PycharmProjects/Tubulent_inflow/2000/data.npy", data_resize, allow_pickle=True, fix_imports=True)
