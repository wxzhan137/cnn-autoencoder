#!/usr/bin/env bash

#SBATCH --job-name=cnnauto-gpu-shared
#SBATCH --account=use300
#SBATCH --partition=gpu-shared
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=6
#SBATCH --gres=gpu:1
#SBATCH --time=00:30:00
#SBATCH --output=tensorflow-gpu-shared.o%j.%N

module purge
module list
printenv

time -p singularity exec --bind /oasis,/scratch --nv /share/apps/gpu/singularity/images/tensorflow/tensorflow-v2.3.0-gpu-20200929.simg python3 cnnmlp_model.py
~
~
~
~
~
~