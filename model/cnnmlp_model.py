from numpy import load
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, AveragePooling2D, Reshape, UpSampling2D
from keras.preprocessing.image import ImageDataGenerator


# Create model
CNNencoder = Sequential()

#add model layers

# First conv2d Output size (96, 256, 16)
CNNencoder.add(Conv2D(16, kernel_size=3, activation='tanh', input_shape=(96, 256, 4), data_format="channels_last", padding="same"))
# Second conv2d Output size (96, 256, 16)
CNNencoder.add(Conv2D(16, kernel_size=3, activation='tanh', padding="same"))
# First AveragePooling 2D Output size (48, 128, 16)
CNNencoder.add(AveragePooling2D(pool_size=(2, 2), strides=(2, 2)))
# Third cond2d Output size (48, 128, 8)
CNNencoder.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# Fourth cond2d Output size (48, 128, 8)
CNNencoder.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# Second AveragePooling 2d Output size (24, 64, 8)
CNNencoder.add(AveragePooling2D(pool_size=(2, 2), strides=(2, 2)))
# Fifth cond2d Output size (24, 64, 8)
CNNencoder.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# Sixth cond2d Output size (24, 64, 8)
CNNencoder.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# Third AveragePooling 2d Output size (12, 32, 8)
CNNencoder.add(AveragePooling2D(pool_size=(2, 2), strides=(2, 2)))
# First reshape Output size (1, 3072)
CNNencoder.add(Flatten())
# First MLP Output size (3072)
CNNencoder.add(Dense(3072, activation='tanh'))
# Second MLP Output size (3072)
CNNencoder.add(Dense(3072, activation='tanh'))
# Second reshape Output size (12, 32, 8)
CNNencoder.add(Reshape((12, 32, 8)))
# Seventh cond2d Output size (12, 32, 8)
CNNencoder.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# Eighth cond2d Output size (12, 32, 8)
CNNencoder.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# First Upsampling 2d Output size (24, 64, 8)
CNNencoder.add(UpSampling2D(size=(2, 2)))
# Ninth cond2d Output size (24, 64, 8)
CNNencoder.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# 10th cond2d Output size (24, 64, 8)
CNNencoder.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# Second Upsampling 2d Output size (48, 128, 8) ?? wrong in the paper ??
CNNencoder.add(UpSampling2D(size=(2, 2)))
# 11th cond2d Output size (48, 128, 16)
CNNencoder.add(Conv2D(16, kernel_size=3, activation='tanh', padding="same"))
# 12th cond2d Output size (48, 128, 16)
CNNencoder.add(Conv2D(16, kernel_size=3, activation='tanh', padding="same"))
# Third Upsampling 2d Output size (96, 256, 16)
CNNencoder.add(UpSampling2D(size=(2, 2)))
# 13th cond2d Output size (96, 256, 4)
CNNencoder.add(Conv2D(4, kernel_size=3, activation='tanh', padding="same"))

# Compile model using accuracy to measure model performance
CNNencoder.compile(optimizer="adam", loss="mean_absolute_error", metrics=['accuracy'])

CNNencoder.summary()

# load data
## normalize ??
datagen = ImageDataGenerator(rescale=1.0/255.0,
                             featurewise_center=True,
                             featurewise_std_normalization=True)
data = load("/Users/wzhan/PycharmProjects/Tubulent_inflow/data/2000/data.npy")

indices = np.random.permutation(data.shape[0])
print("data shape", data.shape)
training_idx, test_idx = indices[:int(0.8*data.shape[0])], indices[int(0.8*data.shape[0]):]
print("indice", training_idx.shape, test_idx.shape)
training_sect, test_sect = data[training_idx, :, :, :], data[test_idx, :, :, :]
train_iterator = datagen.flow(training_sect, training_sect, batch_size=32, shuffle=True)
test_iterator = datagen.flow(test_sect, test_sect, batch_size=32, shuffle=True)
CNNencoder.fit_generator(train_iterator,
               epochs=20,
               steps_per_epoch=len(train_iterator))
_, acc = CNNencoder.evaluate_generator(test_iterator, steps=len(test_iterator), verbose=0)
print('Test Accuracy: %.3f' % (acc * 100))

# save model
model_json = CNNencoder.to_json()
with open("cnnencoder.json", "w") as json_file:
    json_file.write(model_json)

CNNencoder.save_weights('/Users/wzhan/PycharmProjects/Tubulent_inflow/cnn-autoencoder/cnnencoder.h5')
print("Saved the model")



